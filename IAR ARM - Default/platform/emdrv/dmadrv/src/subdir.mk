################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/emdrv/dmadrv/src/dmadrv.c 

OBJS += \
./platform/emdrv/dmadrv/src/dmadrv.o 

C_DEPS += \
./platform/emdrv/dmadrv/src/dmadrv.d 


# Each subdirectory must supply rules for building sources it contributes
platform/emdrv/dmadrv/src/dmadrv.o: ../platform/emdrv/dmadrv/src/dmadrv.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m platform/emdrv/dmadrv/src/dmadrv.d
	@echo 'Finished building: $<'
	@echo ' '


