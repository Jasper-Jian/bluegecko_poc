################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application_properties.c \
../gatt_db.c \
../init_app.c \
../init_board.c \
../init_mcu.c \
../main.c \
../pti.c 

OBJS += \
./application_properties.o \
./gatt_db.o \
./init_app.o \
./init_board.o \
./init_mcu.o \
./main.o \
./pti.o 

C_DEPS += \
./application_properties.d \
./gatt_db.d \
./init_app.d \
./init_board.d \
./init_mcu.d \
./main.d \
./pti.d 


# Each subdirectory must supply rules for building sources it contributes
application_properties.o: ../application_properties.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m application_properties.d
	@echo 'Finished building: $<'
	@echo ' '

gatt_db.o: ../gatt_db.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m gatt_db.d
	@echo 'Finished building: $<'
	@echo ' '

init_app.o: ../init_app.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m init_app.d
	@echo 'Finished building: $<'
	@echo ' '

init_board.o: ../init_board.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m init_board.d
	@echo 'Finished building: $<'
	@echo ' '

init_mcu.o: ../init_mcu.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m init_mcu.d
	@echo 'Finished building: $<'
	@echo ' '

main.o: ../main.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m main.d
	@echo 'Finished building: $<'
	@echo ' '

pti.o: ../pti.c
	@echo 'Building file: $<'
	@echo 'Invoking: IAR C/C++ Compiler for ARM'
	iccarm "$<" -o "$@" --enum_is_int --no_wrap_diagnostics -I"C:\Firmware_Repo\BlueGecko_POC" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\src" -I"C:\Firmware_Repo\BlueGecko_POC\platform\CMSIS\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Include" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emlib\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\bsp" -I"C:\Firmware_Repo\BlueGecko_POC\platform\Device\SiliconLabs\EFR32BG12P\Source\IAR" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\common" -I"C:\Firmware_Repo\BlueGecko_POC\protocol\bluetooth_2.6\ble_stack\inc\soc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\bootloader\api" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\halconfig" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\common\drivers" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\common\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\dmadrv\src" -I"C:\Firmware_Repo\BlueGecko_POC\hardware\kit\EFR32BG12_BRD4103A\config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\uartdrv\inc" -I"C:\Firmware_Repo\BlueGecko_POC\app\bluetooth_2.6\common\stack_bridge" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\common" -I"C:\Firmware_Repo\BlueGecko_POC\iar" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\sleep\inc" -I"C:\Firmware_Repo\BlueGecko_POC\platform\halconfig\inc\hal-config" -I"C:\Firmware_Repo\BlueGecko_POC\platform\radio\rail_lib\chip\efr32" -I"C:\Firmware_Repo\BlueGecko_POC\platform\emdrv\gpiointerrupt\inc" -e --cpu Cortex-M4F --fpu VFPv4_sp --debug --endian little --cpu_mode thumb -On --no_cse --no_unroll --no_inline --no_code_motion --no_tbaa --no_clustering --no_scheduling '-DHAL_CONFIG=1' '-D__NO_SYSTEM_INIT=1' '-DEFR32BG12P332F1024GL125=1' --diag_suppress pa050 --diag_suppress pa050 --diag_error pe223 --diag_error pe223 -I "C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.5/arm/CMSIS/Include" --dependencies=m pti.d
	@echo 'Finished building: $<'
	@echo ' '


