// Copyright 2018 Silicon Laboratories, Inc.
//
//

/********************************************************************
 * Autogenerated file, do not edit.
 *******************************************************************/

#ifndef __GATT_DB_H
#define __GATT_DB_H

#include "bg_gattdb_def.h"

extern const struct bg_gattdb_def bg_gattdb_data;

#define gattdb_service_changed_char             3
#define gattdb_device_name                      7
#define gattdb_peripheral_privacy_flag         11
#define gattdb_tx_power_level                  21
#define gattdb_TestCharacteristic              24
#define gattdb_NonUserCharac                   27
#define gattdb_ServerToClientTx                31
#define gattdb_ClientToServerRx                33
#define gattdb_KeepAlive                       35

#endif
