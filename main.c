/***********************************************************************************************//**
 * \file   main.c
 * \brief  Silicon Labs Empty Example Project
 *
 * This example demonstrates the bare minimum needed for a Blue Gecko C application
 * that allows Over-the-Air Device Firmware Upgrading (OTA DFU). The application
 * starts advertising after boot and restarts advertising after a connection is closed.
 ***************************************************************************************************
 * <b> (C) Copyright 2016 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

/* Board headers */
#include "init_mcu.h"
#include "init_board.h"
#include "init_app.h"
#include "boards.h"
#include "ble-configuration.h"
#include "board_features.h"

/* Bluetooth stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "aat.h"

/* Libraries containing default Gecko configuration values */
#include "em_emu.h"
#include "em_cmu.h"

/* Device initialization header */
#include "hal-config.h"
#include <stdio.h>
#include "retargetswo.h"
#if defined(HAL_CONFIG)
#include "bsphalconfig.h"
#else
#include "bspconfig.h"
#endif

/***********************************************************************************************//**
 * @addtogroup Application
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup app
 * @{
 **************************************************************************************************/

#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS 4
#define DATA_ARRAY_LENGTH 240
#endif
uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];

// Gecko configuration parameters (see gecko_configuration.h)
static const gecko_configuration_t config = {
  .config_flags = 0,
  .sleep.flags = SLEEP_FLAGS_DEEP_SLEEP_ENABLE,
  .bluetooth.max_connections = MAX_CONNECTIONS,
  .bluetooth.heap = bluetooth_stack_heap,
  .bluetooth.heap_size = sizeof(bluetooth_stack_heap),
  .bluetooth.sleep_clock_accuracy = 100, // ppm
  .gattdb = &bg_gattdb_data,
  .ota.flags = 0,
  .ota.device_name_len = 3,
  .ota.device_name_ptr = "OTA",
#if (HAL_PA_ENABLE) && defined(FEATURE_PA_HIGH_POWER)
  .pa.config_enable = 1, // Enable high power PA
  .pa.input = GECKO_RADIO_PA_INPUT_VBAT, // Configure PA input to VBAT
#endif // (HAL_PA_ENABLE) && defined(FEATURE_PA_HIGH_POWER)
};

// Flag for indicating DFU Reset must be performed
uint8_t boot_to_dfu = 0;

uint8 dataArraySend[DATA_ARRAY_LENGTH];
uint8 dataArrayReceive[DATA_ARRAY_LENGTH];

/**
 * @brief  Main function
 */

void printEventToConsole(uint32_t eventId)
{
	switch(eventId)
	{
	  case gecko_evt_dfu_boot_id:
		printf("gecko_evt_dfu_boot_id\n");
		break;

	  case gecko_evt_dfu_boot_failure_id:
		printf("gecko_evt_dfu_boot_failure_id\n");
		break;

	  case gecko_evt_system_boot_id:
		printf("gecko_evt_system_boot_id\n");
		break;

	  case gecko_evt_system_external_signal_id:
		printf("gecko_evt_system_external_signal_id\n");
		break;

	  case gecko_evt_system_awake_id:
		printf("gecko_evt_system_awake_id\n");
		break;

	  case gecko_evt_system_hardware_error_id:
		printf("gecko_evt_system_hardware_error_id\n");
		break;

	  case gecko_evt_le_gap_scan_response_id:
		printf("gecko_evt_le_gap_scan_response_id\n");
		break;

	  case gecko_evt_le_gap_adv_timeout_id:
		printf("gecko_evt_le_gap_adv_timeout_id\n");
		break;

	  case gecko_evt_le_gap_scan_request_id:
		printf("gecko_evt_le_gap_scan_request_id\n");
		break;

	  case gecko_evt_le_gap_bt5_adv_timeout_id:
		printf("gecko_evt_le_gap_bt5_adv_timeout_id\n");
		break;

	  case gecko_evt_le_connection_opened_id:
		printf("gecko_evt_le_connection_opened_id\n");
		break;

	  case gecko_evt_le_connection_closed_id:
		printf("gecko_evt_le_connection_closed_id\n");
		break;

	  case gecko_evt_le_connection_parameters_id:
		printf("gecko_evt_le_connection_parameters_id\n");
		break;

	  case gecko_evt_le_connection_rssi_id:
		printf("gecko_evt_le_connection_rssi_id\n");
		break;

	  case gecko_evt_le_connection_phy_status_id:
		printf("gecko_evt_le_connection_phy_status_id\n");
		break;

	  case gecko_evt_le_connection_bt5_opened_id:
		printf("gecko_evt_le_connection_bt5_opened_id\n");
		break;

	  case gecko_evt_gatt_mtu_exchanged_id:
		printf("gecko_evt_gatt_mtu_exchanged_id\n");
		break;

	  case gecko_evt_gatt_service_id:
		printf("gecko_evt_gatt_service_id\n");
		break;

	  case gecko_evt_gatt_characteristic_id:
		printf("gecko_evt_gatt_characteristic_id\n");
		break;

	  case gecko_evt_gatt_descriptor_id:
		printf("gecko_evt_gatt_descriptor_id\n");
		break;

	  case gecko_evt_gatt_characteristic_value_id:
		printf("gecko_evt_gatt_characteristic_value_id\n");
		break;

	  case gecko_evt_gatt_descriptor_value_id:
		printf("gecko_evt_gatt_descriptor_value_id\n");
		break;

	  case gecko_evt_gatt_procedure_completed_id:
		printf("gecko_evt_gatt_procedure_completed_id\n");
		break;

	  case gecko_evt_gatt_server_attribute_value_id:
		printf("gecko_evt_gatt_server_attribute_value_id\n");
		break;

	  case gecko_evt_gatt_server_user_read_request_id:
		printf("gecko_evt_gatt_server_user_read_request_id\n");
		break;

	  case gecko_evt_gatt_server_user_write_request_id:
		printf("gecko_evt_gatt_server_user_write_request_id\n");
		break;

	  case gecko_evt_gatt_server_characteristic_status_id:
		printf("gecko_evt_gatt_server_characteristic_status_id\n");
		break;

	  case gecko_evt_gatt_server_execute_write_completed_id:
		printf("gecko_evt_gatt_server_execute_write_completed_id\n");
		break;

	  case gecko_evt_endpoint_status_id:
		printf("gecko_evt_endpoint_status_id\n");
		break;

	  case gecko_evt_hardware_soft_timer_id:
		printf("gecko_evt_hardware_soft_timer_id\n");
		break;

	  case gecko_evt_flash_ps_key_id:
		printf("gecko_evt_flash_ps_key_id\n");
		break;

	  case gecko_evt_test_dtm_completed_id:
		printf("gecko_evt_test_dtm_completed_id\n");
		break;

	  case gecko_evt_test_hcidump_id:
		printf("gecko_evt_test_hcidump_id\n");
		break;

	  case gecko_evt_sm_passkey_display_id:
		printf("gecko_evt_sm_passkey_display_id\n");
		break;

	  case gecko_evt_sm_passkey_request_id:
		printf("gecko_evt_sm_passkey_request_id\n");
		break;

	  case gecko_evt_sm_confirm_passkey_id:
		printf("gecko_evt_sm_confirm_passkey_id\n");
		break;

	  case gecko_evt_sm_bonded_id:
		printf("gecko_evt_sm_bonded_id\n");
		break;

	  case gecko_evt_sm_bonding_failed_id:
		printf("gecko_evt_sm_bonding_failed_id\n");
		break;

	  case gecko_evt_sm_list_bonding_entry_id:
		printf("gecko_evt_sm_list_bonding_entry_id\n");
		break;

	  case gecko_evt_sm_list_all_bondings_complete_id:
		printf("gecko_evt_sm_list_all_bondings_complete_id\n");
		break;

	  case gecko_evt_sm_confirm_bonding_id:
		printf("gecko_evt_sm_confirm_bonding_id\n");
		break;

	  case gecko_evt_homekit_setupcode_display_id:
		printf("gecko_evt_homekit_setupcode_display_id\n");
		break;

	  case gecko_evt_homekit_paired_id:
		printf("gecko_evt_homekit_paired_id\n");
		break;

	  case gecko_evt_homekit_pair_verified_id:
		printf("gecko_evt_homekit_pair_verified_id\n");
		break;

	  case gecko_evt_homekit_connection_opened_id:
		printf("gecko_evt_homekit_connection_opened_id\n");
		break;

	  case gecko_evt_homekit_connection_closed_id:
		printf("gecko_evt_homekit_connection_closed_id\n");
		break;

	  case gecko_evt_homekit_identify_id:
		printf("gecko_evt_homekit_identify_id\n");
		break;

	  case gecko_evt_homekit_write_request_id:
		printf("gecko_evt_homekit_write_request_id\n");
		break;

	  case gecko_evt_homekit_read_request_id:
		printf("gecko_evt_homekit_read_request_id\n");
		break;

	  case gecko_evt_homekit_error_id:
		printf("gecko_evt_homekit_error_id\n");
		break;

	  case gecko_evt_homekit_pairing_removed_id:
		printf("gecko_evt_homekit_pairing_removed_id\n");
		break;

	  case gecko_evt_user_message_to_host_id:
		printf("gecko_evt_user_message_to_host_id\n");
		break;

	  default:
		  printf("Event is: %d \n", eventId);
		  break;

	}
}

void InitialiseIOs(void)
{
	GPIO_PinModeSet(gpioPortD, 7, gpioModePushPull, 1);
}

void main(void)
{
  // Initialize device
  initMcu();
  // Initialize board
  initBoard();

  InitialiseIOs();

  RETARGET_SwoInit();
  // Initialize application
  initApp();

  // Initialize stack
  gecko_init(&config);

  //Start of Jasper testing code
  int8_t count = -1;
  uint32_t eventArray[5] = {123,123,123,123,123};

  memset(dataArraySend, 1, DATA_ARRAY_LENGTH);
  memset(dataArrayReceive, 2, DATA_ARRAY_LENGTH);


  //End of Jasper testing code

  while (1) {
    /* Event pointer for handling events */
    struct gecko_cmd_packet* evt;

    /* Check for stack event. */
    evt = gecko_wait_event();
    uint32_t event = BGLIB_MSG_ID(evt->header);
    printEventToConsole(event);
    if(event == gecko_evt_le_connection_opened_id)
    {
    	//start logging events
    	count = 0;
    }

    if(event == gecko_evt_gatt_server_user_write_request_id)
    {
    	uint8_t dummyCount = 0;
    	dummyCount++;
    }
    else if(event == gecko_evt_gatt_server_execute_write_completed_id)
    {
    	uint8_t dummyCount = 0;
    	dummyCount++;
    }


    if(count >= 0)
    {
    	eventArray[count] = event;
    	count++;
    	if(count >= 5)
    	{
    		count = 0;
    	}
    }

    uint16_t characteristic;
    /* Handle events */
    switch (BGLIB_MSG_ID(evt->header)) {
      /* This boot event is generated when the system boots up after reset.
       * Do not call any stack commands before receiving the boot event.
       * Here the system is set to start advertising immediately after boot procedure. */
      case gecko_evt_system_boot_id:

        /* Set advertising parameters. 100ms advertisement interval. All channels used.
         * The first two parameters are minimum and maximum advertising interval, both in
         * units of (milliseconds * 1.6). The third parameter '7' sets advertising on all channels. */
        gecko_cmd_le_gap_set_adv_parameters(160, 160, 7);

        /*Set the TX power*/
        gecko_cmd_system_set_tx_power(100);

        /* Start general advertising and enable connections. */
        gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);

        /*Set connection parameters*/
        gecko_cmd_le_gap_set_conn_parameters(10, 10, 0, 100);
        break;

      case gecko_evt_le_connection_closed_id:

        /* Check if need to boot to dfu mode */
        if (boot_to_dfu) {
          /* Enter to DFU OTA mode */
          gecko_cmd_system_reset(2);
        } else {
          /* Restart advertising after client has disconnected */
          gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
        }
        break;

      /* Events related to OTA upgrading
         ----------------------------------------------------------------------------- */

      /* Check if the user-type OTA Control Characteristic was written.
       * If ota_control was written, boot the device into Device Firmware Upgrade (DFU) mode. */
      case gecko_evt_gatt_server_user_write_request_id:

    	  characteristic = evt->data.evt_gatt_server_user_write_request.characteristic;
    	  if (evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_peripheral_privacy_flag)
    	  {

    	  }

/*        if (evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
           Set flag to enter to OTA mode
          boot_to_dfu = 1;
           Send response to Write Request
          gecko_cmd_gatt_server_send_user_write_response(
            evt->data.evt_gatt_server_user_write_request.connection,
            gattdb_ota_control,
            bg_err_success);

           Close connection to enter to DFU OTA mode
          gecko_cmd_endpoint_close(evt->data.evt_gatt_server_user_write_request.connection);
        }*/
    	  if(evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_TestCharacteristic)
    	  {
        	  struct gecko_msg_gatt_server_write_attribute_value_rsp_t* writeResponse = gecko_cmd_gatt_server_write_attribute_value(
        			  gattdb_TestCharacteristic, evt->data.evt_gatt_server_user_write_request.offset, evt->data.evt_gatt_server_user_write_request.value.len,
					  evt->data.evt_gatt_server_user_write_request.value.data);

        	  struct gecko_msg_gatt_server_send_user_write_response_rsp_t* responsePtr;
        	  responsePtr = gecko_cmd_gatt_server_send_user_write_response(
        			  evt->data.evt_gatt_server_user_write_request.connection, evt->data.evt_gatt_server_user_write_request.characteristic, writeResponse->result);

    	  }

    	  if(evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ClientToServerRx)
    	  {
    		  gecko_cmd_gatt_server_send_user_write_response(
    				  evt->data.evt_gatt_server_user_write_request.connection, evt->data.evt_gatt_server_user_write_request.characteristic, 0);
    		  memcpy(dataArrayReceive, evt->data.evt_gatt_server_user_write_request.value.data, evt->data.evt_gatt_server_user_write_request.value.len);
    	  }
          break;

      case gecko_evt_gatt_server_user_read_request_id:

    	  if(evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ClientToServerRx)
    	  {
    		  gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
    		          evt->data.evt_gatt_server_user_read_request.characteristic, 0, DATA_ARRAY_LENGTH, &dataArrayReceive[0]);
    	  }

    	  if(evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_ServerToClientTx)
    	  {
        	  gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
        			  evt->data.evt_gatt_server_user_read_request.characteristic, 0, DATA_ARRAY_LENGTH, &dataArraySend[0]);
    	  }

    	  if(evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_KeepAlive)
    	  {
    		  uint8 dataByte = 123;
        	  gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,
        			  evt->data.evt_gatt_server_user_read_request.characteristic, 0, 1, &dataByte);
    	  }

    	  break;

      case gecko_evt_gatt_server_attribute_value_id:
    	  struct gecko_msg_gatt_server_attribute_value_evt_t attribute_cmd_packet;
    	  attribute_cmd_packet = evt->data.evt_gatt_server_attribute_value;
    	  struct gecko_msg_gatt_server_read_attribute_value_rsp_t* rspPacket = gecko_cmd_gatt_server_read_attribute_value(
    			  gattdb_NonUserCharac, evt->data.evt_gatt_server_attribute_value.offset);
    	  break;

      case gecko_evt_sm_bonded_id:

    	  break;

      default:
        break;
    }
  }
}

/** @} (end addtogroup app) */
/** @} (end addtogroup Application) */
